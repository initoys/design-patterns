package
{
	import flash.display.Sprite;
	
	import printcenters.HighVolPrintCenter;
	import printcenters.LowVolPrintCenter;
	import printcenters.MultifunctionPrintCenter;
	import printcenters.NewHighVolPrintCenter;
	import printcenters.NewLowVolPrintCenter;
	import printcenters.NewPrintCenter;
	import printcenters.PrintCenter;
	
	public class Main extends Sprite
	{
		public function Main()
		{
			var pcHighVol:PrintCenter = new HighVolPrintCenter();
			var pcLowVol:PrintCenter = new LowVolPrintCenter();
			var pcMultiVol:PrintCenter = new MultifunctionPrintCenter();
			
			pcHighVol.print("longThesis.doc");
			pcLowVol.print("shortVita.doc");
			pcMultiVol.print("multi.indd");
			
			var pcNewHighVol:NewPrintCenter = new NewHighVolPrintCenter(); 
			var pcNewLowhVol:NewPrintCenter = new NewLowVolPrintCenter();
			
			pcNewHighVol.print("LongThesis.doc", NewHighVolPrintCenter.BW);
			pcNewHighVol.print("SalesReport.pdf", NewHighVolPrintCenter.COLOR);
			pcNewLowhVol.print("ShortVita.doc", NewLowVolPrintCenter.BW);
			pcNewLowhVol.print("SalesChart.xlc", NewLowVolPrintCenter.COLOR);
		}
	}
}