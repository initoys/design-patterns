package printcenters
{
	import flash.errors.IllegalOperationError;

	// ABSTRACT Class (should be subclassed and not instantiated)
	public class PrintCenter
	{
		public function print(fn:String):void
		{
			var printJob:IPrintjob = this.createPrintjob();
			printJob.start(fn);
		}
		
		// ABSTRACT method (must be overridden by a subclass)
		protected function createPrintjob():IPrintjob
		{
			throw new IllegalOperationError("Abstract method: must be overridden by a subclass");
			return null;
		}
	}
}