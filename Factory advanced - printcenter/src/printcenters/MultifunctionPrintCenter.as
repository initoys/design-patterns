package printcenters
{
	public class MultifunctionPrintCenter extends PrintCenter
	{
		override protected function createPrintjob():IPrintjob {
			trace("Creating new printjob for the multi printer");
			return new Multifunctionrintjob();
		}
	}
}