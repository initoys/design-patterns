package printcenters
{
	public class HighVolPrintCenter extends PrintCenter
	{
		override protected function createPrintjob():IPrintjob {
			trace("Creating new printjob for the workgroup printer");
			return new WorkgroupPrintjob();
		}
	}
}