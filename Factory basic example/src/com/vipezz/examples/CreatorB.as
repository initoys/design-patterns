package com.vipezz.examples
{
	public class CreatorB extends Creator
	{
		override protected function factoryMethod():IProduct {
			trace("Creating product 2");
			return new Product2(); //Returns concrete product
		}
	}
}