package com.vipezz.examples
{
	public interface IProduct
	{
		function manipulate():void;
	}
}